#!/bin/sh

set -e ##This line is used so that during the execution of the script if any of the lines fail, return a failure message on the screen.

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'