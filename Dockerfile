FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="samhavvin@gmail.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nignx/uwsgi_params

ENV LISTEN_PORT=8000
ENV  APP_HOST=app
ENV APP_PORT=9000

USER root

RUN mkdir -p /vol/static
##Line 13 will create a directory (/vol/static) -p will create "vol" if it doesnt exist already
RUN chmod 755 /vol/static
##Line 15 is setting permissions of the directory
RUN touch /etc/nginx/conf.d/default.conf
##Line 17 will create a empty file inside conf.d. The name of the file is default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf
##Line 19 sets the permission of this file 

COPY ./entrypoint.sh /entrypoint.sh
##Line 22 will copy the content from the entrypoint file and 
RUN chmod +x /entrypoint.sh
##Line 23 makes /entrypoint.sh executable

USER nginx

CMD ["/entrypoint.sh"]